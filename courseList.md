# Course list

List of online courses or moocs which I'm planning to attend

| Course Name                              | Platform | Done | Date |
| :--------------------------------------- | :------- | :--- | ---- |
| [Web Design for Everybody (Basics of Web Development and Coding)](https://www.coursera.org/specializations/web-design) | Coursera | N    |      |
| [Functional Programming in Scala](https://www.coursera.org/specializations/scala) | Coursera | N    |      |
| [Java Programming: Object-Oriented Design of Data Structures](https://www.coursera.org/specializations/java-object-oriented) | Coursera | N    |      |
| [Data Mining](https://www.coursera.org/specializations/data-mining) | Coursera | N    |      |
| [iOS App Development with Swift](https://www.coursera.org/specializations/app-development) | Coursera | N    |      |
| [Ruby on Rails Web Development](https://www.coursera.org/specializations/ruby-on-rails) | Coursera | N    |      |
| [Big Data](https://www.coursera.org/specializations/bigdata) | Coursera | N    |      |
| [Responsive Website Development and Design](https://www.coursera.org/specializations/website-development) | Coursera | N    |      |
| [Photography Basics and Beyond: From Smartphone to DSLR](https://www.coursera.org/specializations/photography-basics) | Coursera | N    |      |
| [Wie funktioniert das Internet?](https://open.hpi.de/courses/internetworking2016) | Open HPI | N    |      |
| [Code of Life – When Computer Science Meets Genetics](https://open.hpi.de/courses/ehealth2016) | Open HPI | N    |      |
| [Agile Development Using Ruby on Rails](https://www.edx.org/xseries/agile-development-using-ruby-rails) | Edx      | N    |      |
|                                          |          |      |      |



Courses i have alredy completed

| Course Name                              | Platform | Done | Date |
| :--------------------------------------- | :------- | :--- | ---- |
| [Web Technologies](https://open.hpi.de/courses/webtech2015) | Open HPI | J    | 2015 |
| [Java für Einsteiger](https://open.hpi.de/courses/javaeinstieg2015) | Open HPI | J    | 2015 |
| [Sicherheit im Internet](https://open.hpi.de/courses/intsec2016) | Open HPI | J    | 2016 |
| [Social Media - What No One has Told You about Privacy](https://open.hpi.de/courses/ws-privacy2016) | Open HPI | J    | 2016 |
| [HTML5 Part 1: HTML5 Coding Essentials and Best Practices](https://courses.edx.org/courses/course-v1:W3Cx+HTML5.1x+2T2016) | Edx      | J    | 2016 |
|                                          |          |      |      |
|                                          |          |      |      |
|                                          |          |      |      |
|                                          |          |      |      |